<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210306194422 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE distributed_task (id INT AUTO_INCREMENT NOT NULL, task_id INT NOT NULL, user_id INT DEFAULT NULL, current_status_id INT DEFAULT NULL, time_from DATETIME DEFAULT NULL, time_to DATETIME DEFAULT NULL, created_at DATETIME DEFAULT NULL, task_status INT NOT NULL, INDEX IDX_FE8250138DB60186 (task_id), INDEX IDX_FE825013B0D1B111 (current_status_id), INDEX IDX_FE825013A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) DEFAULT NULL, nickname VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE distributed_task ADD CONSTRAINT FK_FE8250138DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE distributed_task ADD CONSTRAINT FK_FE825013B0D1B111 FOREIGN KEY (current_status_id) REFERENCES task_status (id)');
        $this->addSql('ALTER TABLE distributed_task ADD CONSTRAINT FK_FE825013A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE distributed_task DROP FOREIGN KEY FK_FE8250138DB60186');
        $this->addSql('ALTER TABLE distributed_task DROP FOREIGN KEY FK_FE825013B0D1B111');
        $this->addSql('ALTER TABLE distributed_task DROP FOREIGN KEY FK_FE825013A76ED395');
        $this->addSql('DROP TABLE distributed_task');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE task_status');
        $this->addSql('DROP TABLE user');
    }
}
