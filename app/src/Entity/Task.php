<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN')"},
 *     formats={"json"})
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity=DistributedTask::class, mappedBy="task")
     */
    private $distributed_task;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
//        $this->distributed_task = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updated_at = new \DateTime();
    }

//    /**
//     * @return Collection|DistributedTask[]
//     */
//    public function getDistributedTask(): Collection
//    {
//        return $this->distributed_task;
//    }
//
//    public function addDistributedTask(DistributedTask $distributedTask): self
//    {
//        if (!$this->distributed_task->contains($distributedTask)) {
//            $this->distributed_task[] = $distributedTask;
//            $distributedTask->setTask($this);
//        }
//
//        return $this;
//    }
//
//    public function removeDistributedTask(DistributedTask $distributedTask): self
//    {
//        if ($this->distributed_task->removeElement($distributedTask)) {
//            // set the owning side to null (unless already changed)
//            if ($distributedTask->getTask() === $this) {
//                $distributedTask->setTask(null);
//            }
//        }
//
//        return $this;
//    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

}
