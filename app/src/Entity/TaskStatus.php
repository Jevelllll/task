<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TaskStatusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations = {
 *      "post"={"access_control"="is_granted('ROLE_ADMIN')"},
 *      "get"
 *     },
 *     itemOperations = {
 *       "put"={"access_control"="is_granted('ROLE_ADMIN')"},
 *       "PATCH"={"access_control"="is_granted('ROLE_ADMIN')"},
 *       "delete"={"access_control"="is_granted('ROLE_ADMIN')"},
 *       "get"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     },
 *     formats={"json"})
 * @ORM\Entity(repositoryClass=TaskStatusRepository::class)
 */
class TaskStatus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
