<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DistributedTaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ApiResource(
 *     collectionOperations = {
 *      "post"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations = {
 *       "put"={"access_control"="is_granted('ROLE_ADMIN')"},
 *       "PATCH"={"access_control"="is_granted('ROLE_ADMIN')"},
 *       "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
 *     },
 *     formats={"json"})
 * @ORM\Entity(repositoryClass=DistributedTaskRepository::class)
 */
/**
 * @ORM\Entity(repositoryClass=DistributedTaskRepository::class)
 */
class DistributedTask
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $task_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $task_status;

    /**
     * @ORM\ManyToOne(targetEntity=Task::class)
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="TaskStatus")
     * @ORM\JoinColumn(name="task_status", referencedColumnName="id")
     */
    private $current_status;

    /**
     * @return mixed
     */
    public function getCurrentStatus(): ?TaskStatus
    {
        return $this->current_status;
    }

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="distributed_task")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time_from;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time_to;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;


    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTask(): ?Task
    {
        return $this->task;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getTimeFrom(): ?\DateTimeInterface
    {
        return $this->time_from;
    }

    public function setTimeFrom(?\DateTimeInterface $time_from): self
    {
        $this->time_from = $time_from;

        return $this;
    }

    public function getTimeTo(): ?\DateTimeInterface
    {
        return $this->time_to;
    }

    public function setTimeTo(?\DateTimeInterface $time_to): self
    {
        $this->time_to = $time_to;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTaskStatus(): ?int
    {
        return $this->task_status;
    }

    public function setTaskStatus(int $task_status): self
    {
        $this->task_status = $task_status;

        return $this;
    }



    public function getTaskId(): ?int
    {
        return $this->task_id;
    }

    public function setTaskId(int $task_id): self
    {
        $this->task_id = $task_id;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function setTask(?Task $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function setCurrentStatus(?TaskStatus $current_status): self
    {
        $this->current_status = $current_status;

        return $this;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
