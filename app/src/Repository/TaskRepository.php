<?php

namespace App\Repository;

use App\Entity\DistributedTask;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{

    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Task::class);
        $this->manager = $manager;
    }

    public function free_tasks($user_id)
    {

        $queryDistributed = $this->manager->createQueryBuilder()
            ->select('p.task_id')
            ->from('APP\Entity\DistributedTask', 'p')
            ->andWhere('p.user_id = :user_id')
            ->setParameter('user_id', $user_id)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $query = $this->manager->createQueryBuilder()
            ->select('p')
            ->from('App\Entity\Task', 'p');
        if (!empty($queryDistributed)) {
            $query = $query->andWhere('p.id NOT IN(:tasksIds)')
                ->setParameter('tasksIds', array_column($queryDistributed, 'task_id'));
        }
        $query = $query->getQuery()->getResult(Query::HYDRATE_ARRAY);
        return $query;
    }

    public function started($user_id, $status_id)
    {

        $sql = 'SELECT p.id, p.task_id, p.user_id, p.created_at,
             u.email, u.nickname, c.id as status_id,c.name as status_name,
              cs.name as task_name, cs.description
            FROM App\Entity\DistributedTask p
            INNER JOIN p.user u
            INNER JOIN p.current_status c
            INNER JOIN p.task cs 
            WHERE p.user_id = :user_id';

        $query = $this->manager;
        if ($status_id > 0) {
            $sql .= ' AND c.id  = :task_status';
            $query = $query->createQuery($sql)
                ->setParameter('task_status', $status_id)
                ->setParameter('user_id', $user_id);
        } else {
            $query = $query->createQuery($sql)
                ->setParameter('user_id', $user_id);
        }
        return $query->getResult(Query::HYDRATE_ARRAY);

    }

    // /**
    //  * @return Task[] Returns an array of Task objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Task
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
