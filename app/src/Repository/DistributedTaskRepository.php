<?php

namespace App\Repository;

use App\Entity\DistributedTask;
use App\Entity\Task;
use App\Entity\TaskStatus;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method DistributedTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method DistributedTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method DistributedTask[]    findAll()
 * @method DistributedTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistributedTaskRepository extends ServiceEntityRepository
{

    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, DistributedTask::class);
        $this->manager = $manager;
    }

    public function saveDistributedTask($taskId, $userId): bool
    {
        $taskModel = $this->manager->getRepository(Task::class)->findOneBy(
            ['id' => (int)$taskId]);
        $userModel = $this->manager->getRepository(User::class)->findOneBy(
            ['id' => (int)$userId]);
        $taskStatusModel = $this->manager->getRepository(TaskStatus::class)->findOneBy(
            ['id' => 1]);
        if ($taskModel instanceof Task && $userModel instanceof User) {
            $distributedTask = new DistributedTask();
            $distributedTask
                ->setTask($taskModel)
                ->setUserId((int)$userId)
                ->setCurrentStatus($taskStatusModel)
                ->setUser($userModel)
                ->setTaskStatus(1);

            $this->manager->persist($distributedTask);
            $this->manager->flush();
            return true;
        } else {
            return false;
        }
    }

    public function changeStatus($status, $id) {
        $distributedTask = $this->manager->getRepository(DistributedTask::class)->findOneBy(
            ['id' => (int)$id]);

        if ($distributedTask instanceof DistributedTask) {
            $distributedTask->setTaskStatus($status);
            $this->manager->persist($distributedTask);
            $this->manager->flush();
            return true;
        } else {
            return false;
        }
    }

    public function checkCountSave($taskId, $userId)
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->andWhere('d.task_id = :task_id')
            ->andWhere('d.user_id = :user_id')
            ->setParameter('task_id', $taskId)
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function remove($id): bool
    {

        $distributedTask = $this->manager->getRepository(DistributedTask::class)->findOneBy(
            ['id' => (int)$id]);

        if ($distributedTask) {
            $this->manager->remove($distributedTask);
            $this->manager->flush();
            return true;
        } else {
            return false;
        }
    }

    public function getAll($status_id)
    {
        $sql = 'SELECT p.id, p.task_id, p.user_id, p.created_at,
             u.email, u.nickname, c.id as status_id,c.name as status_name,
              cs.name as task_name, cs.description
            FROM App\Entity\DistributedTask p
            INNER JOIN p.user u
            INNER JOIN p.current_status c
            INNER JOIN p.task cs ';

        $query = $this->manager;
        if ($status_id > 0) {
            $sql .= 'WHERE p.task_status = :task_status';
            $query =  $query->createQuery($sql)->setParameter('task_status', $status_id);
        } else {
            $query =  $query->createQuery($sql);
        }
        return $query->getResult(Query::HYDRATE_ARRAY);
    }
    // /**
    //  * @return DistributedTask[] Returns an array of DistributedTask objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DistributedTask
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
