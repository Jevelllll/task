<?php

namespace App\DataFixtures;

use App\Entity\TaskStatus;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setNickname('Admin');
        $user->setEmail('andmin@admin.com');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($this->userPasswordEncoder->encodePassword($user, 'admin'));

        $manager->persist($user);


        $taskStatusNEw = new TaskStatus();
        $taskStatusNEw->setName('new');
        $manager->persist($taskStatusNEw);


        $taskStatusDone= new TaskStatus();
        $taskStatusDone->setName('done');
        $manager->persist($taskStatusDone);


        $taskStatusInProgress= new TaskStatus();
        $taskStatusInProgress->setName('in progress');
        $manager->persist($taskStatusInProgress);

        $manager->flush();
    }
}
