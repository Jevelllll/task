<?php

namespace App\Controller;

use App\Repository\DistributedTaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DistributedTaskController extends AbstractController
{

    private $distributedTaskRepository;

    public function __construct(DistributedTaskRepository $distributedTaskRepository)
    {
        $this->distributedTaskRepository = $distributedTaskRepository;
    }

    /**
     * @Route("/api/distributed/task/{status_id}", name="distributed_task")
     */
    public function index($status_id = 0): Response
    {
        $hasAccess = $this->isGranted('ROLE_ADMIN');
        if (!$hasAccess) {
            return new JsonResponse(['status' => 'Access Denied.'], Response::HTTP_FORBIDDEN);
        }
        return new JsonResponse($this->distributedTaskRepository->getAll($status_id), Response::HTTP_OK);

    }

    /**
     * @Route("/api/distributed/remove/{id}", name="distributed_task_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function delete(Request $request, $id): Response
    {

        $hasAccess = $this->isGranted('ROLE_ADMIN');

        if (!$hasAccess) {
            return new JsonResponse(['status' => 'Access Denied.'], Response::HTTP_FORBIDDEN);
        }
        if ($this->distributedTaskRepository->remove($id)) {
            return new JsonResponse(['status' => 'delete'], Response::HTTP_CREATED);
        } else {
            return new JsonResponse(['status' => 'not found!'], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @Route("/api/distributed/change-status/{distrId}", name="change-status", methods={"PUT"})
     * @param Request $request
     * @return Response
     */
    public function changeStatus(Request $request, int $distrId): Response
    {
        $data = json_decode($request->getContent(), true);
        if (!empty($data['status_id'])) {
            if ($this->distributedTaskRepository->changeStatus((int)$data['status_id'], $distrId)) {
                return new JsonResponse(['status' => 'update'], Response::HTTP_CREATED);
            } else {
                return new JsonResponse(['status' => 'not found!'], Response::HTTP_NOT_FOUND);
            }
        } else {
            return new JsonResponse(['status' => 'Empty data field'], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Route("/api/distributed/add", name="distributed_add", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $hasAccess = $this->isGranted('ROLE_ADMIN');

        if (!$hasAccess) {
            return new JsonResponse(['status' => 'Access Denied.'], Response::HTTP_FORBIDDEN);
        }
        $data = json_decode($request->getContent(), true);

        if (isset($data['task_id']) && $data['user_id']) {
            $taskId = $data['task_id'];
            $userId = $data['user_id'];
            if (empty($taskId) || empty($userId)) {
                return new JsonResponse(['status' => 'Empty data field'], Response::HTTP_NOT_FOUND);
            }
            $count = $this->distributedTaskRepository->checkCountSave($taskId, $userId);
            if ($count == 0) {
                if ($this->distributedTaskRepository->saveDistributedTask($taskId, $userId)) {
                    return new JsonResponse(['status' => 'created'], Response::HTTP_CREATED);
                } else {
                    return new JsonResponse(['status' => 'not found!'], Response::HTTP_NOT_FOUND);
                }
            } else {
                return new JsonResponse(['status' => 'Duplicate!'], Response::HTTP_CONFLICT);
            }
        } else {
            return new JsonResponse(['status' => 'Expecting mandatory parameters!'], Response::HTTP_NOT_FOUND);
        }
    }
}
