<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/api/users/profile", name="my-profile", methods={"GET"})
     * @return JsonResponse
     */
    public function getMyProfile(): JsonResponse
    {
        $token = $this->get('security.token_storage')->getToken();

        if (null !== $token) {
            $user = $token->getUser()->toArray();
            unset($user['password']);
            unset($user['roles']);
            unset($user['plainPassword']);
            unset($user['updatedAt']);
            unset($user['createdAt']);
            return new JsonResponse($user, Response::HTTP_OK);
        } else {
            return new JsonResponse(['status' => 'not found!'], Response::HTTP_NOT_FOUND);
        }
    }
}