<?php

namespace App\Controller;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{

    private $taskRepository;


    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @Route("/api/tasks/free-tasks/{user_id}", name="free-tasks", methods={"GET"})
     * @param int $user_id
     * @return JsonResponse
     */
    public function free_tasks(int $user_id): JsonResponse
    {
        $hasAccess = $this->isGranted('ROLE_ADMIN');
        if (!$hasAccess) {
            return new JsonResponse(['status' => 'Access Denied.'], Response::HTTP_FORBIDDEN);
        }
        return new JsonResponse($this->taskRepository->free_tasks($user_id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/tasks/started/{user_id}/{task_status}", name="started-tasks", methods={"GET"})
     * @param int $user_id
     * @param int $task_status
     * @return JsonResponse
     */
    public function started(int $user_id, int $task_status = 0): JsonResponse
    {
        return new JsonResponse($this->taskRepository->started($user_id, $task_status), Response::HTTP_OK);
    }
}
