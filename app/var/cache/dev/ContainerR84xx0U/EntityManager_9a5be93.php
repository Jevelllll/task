<?php

namespace ContainerR84xx0U;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder1184a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerc99fd = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties4ef75 = [
        
    ];

    public function getConnection()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getConnection', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getMetadataFactory', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getExpressionBuilder', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'beginTransaction', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getCache', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getCache();
    }

    public function transactional($func)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'transactional', array('func' => $func), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->transactional($func);
    }

    public function commit()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'commit', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->commit();
    }

    public function rollback()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'rollback', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getClassMetadata', array('className' => $className), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'createQuery', array('dql' => $dql), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'createNamedQuery', array('name' => $name), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'createQueryBuilder', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'flush', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'clear', array('entityName' => $entityName), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->clear($entityName);
    }

    public function close()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'close', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->close();
    }

    public function persist($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'persist', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'remove', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'refresh', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'detach', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'merge', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getRepository', array('entityName' => $entityName), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'contains', array('entity' => $entity), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getEventManager', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getConfiguration', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'isOpen', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getUnitOfWork', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getProxyFactory', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'initializeObject', array('obj' => $obj), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'getFilters', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'isFiltersStateClean', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'hasFilters', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return $this->valueHolder1184a->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerc99fd = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder1184a) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder1184a = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder1184a->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__get', ['name' => $name], $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        if (isset(self::$publicProperties4ef75[$name])) {
            return $this->valueHolder1184a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1184a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1184a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1184a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder1184a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__isset', array('name' => $name), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1184a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder1184a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__unset', array('name' => $name), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder1184a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder1184a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__clone', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        $this->valueHolder1184a = clone $this->valueHolder1184a;
    }

    public function __sleep()
    {
        $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, '__sleep', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;

        return array('valueHolder1184a');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerc99fd = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerc99fd;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerc99fd && ($this->initializerc99fd->__invoke($valueHolder1184a, $this, 'initializeProxy', array(), $this->initializerc99fd) || 1) && $this->valueHolder1184a = $valueHolder1184a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder1184a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder1184a;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
