import {Component, OnDestroy, OnInit} from '@angular/core';
import {TasksService} from '../../../services/task/tasks.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {DistributedModel} from '../../../models/DistributedModel';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {TaskStatus} from '../../../models/TaskModel';
import {DistributedService} from '../../../services/distributed/distributed.service';

@Component({
  selector: 'app-started',
  templateUrl: './started.component.html',
  styleUrls: ['./started.component.scss']
})
export class StartedComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  public startedList: DistributedModel[] | undefined;
  public statusList: TaskStatus[] | undefined;
  public statusFormControl!: FormControl;
  public statusForm: FormGroup | any;
  items: FormArray | undefined;

  constructor(private tasksService: TasksService, private formBuilder: FormBuilder,
              private distributedService: DistributedService) {
  }

  ngOnInit(): void {

    this.statusFormControl = new FormControl(0);
    this.statusForm = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
    this.listenerStatusChange();

    this.tasksService.obsEllTasksStatus.pipe(takeUntil(this.destroyed$)).subscribe((result) => {
      if (result.length > 0) {
        this.statusList = result;
      } else {
        this.tasksService.getStatus().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }
    });

    this.tasksService.obsEllTasksStarted.pipe(takeUntil(this.destroyed$)).subscribe((result) => {
      if (result.length > 0) {
        this.startedList = result;
        result.forEach(((value, index) => {
          this.addItem(value?.status_id);
        }));
      } else {
        this.tasksService.getStarted(1).pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }
    });

  }

  private listenerStatusChange(isAsync = true): void {
    if (isAsync) {
      this.statusFormControl.valueChanges.pipe(takeUntil(this.destroyed$)).subscribe((data) => {
        this.startedList = [];
        this.items = this.statusForm.get('items') as FormArray;
        this.items.clear();
        this.sendStatus(data);
      });
    } else {
      const data = this.statusFormControl.value;
      this.startedList = [];
      this.items = this.statusForm.get('items') as FormArray;
      this.items.clear();
      this.sendStatus(data);
    }
  }

  createItem(statusId: any): FormGroup {
    return this.formBuilder.group({
      status_id: statusId,
    });
  }

  addItem(statusId: number): void {
    this.items = this.statusForm.get('items') as FormArray;
    this.items.push(this.createItem(statusId));
  }

  // tslint:disable-next-line:variable-name
  onSubmit(index: number, distrID: any): void {

    this.distributedService.changeStatus(distrID, this.statusForm.value.items[index].status_id)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((result) => {
        this.listenerStatusChange(false);
      });
  }

  sendStatus(statusId: any): void {
    this.tasksService.getStarted(statusId).pipe(takeUntil(this.destroyed$)).subscribe(() => {
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
