import {Component, OnDestroy, OnInit} from '@angular/core';
import {TasksService} from '../../services/task/tasks.service';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TaskInterface} from '../../models/TaskModel';
import {MatDialog} from '@angular/material/dialog';
import {AddEditFormComponent} from './add/add-edit-form/add-edit-form.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  public obsTasks: Observable<TaskInterface[]> | undefined;
  public isAdmin = false;

  constructor(private tasksService: TasksService, public dialog: MatDialog) {
  }

  ngOnInit(): void {

    const helper = new JwtHelperService();
    const roles = helper.decodeToken(localStorage.getItem('jwt') as string).roles;
    if (roles && roles.indexOf('ROLE_ADMIN') > -1) {

      this.isAdmin = true;

      this.tasksService.obsEllTasks.pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          if (data.length > 0) {
            this.obsTasks = new BehaviorSubject<TaskInterface[]>(data).asObservable();
          } else {
            this.tasksService.getAllTasks()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(() => {
              });
          }
        });
    } else {
    }
  }

  deleteTask(id: number, linkElement: any): void {
    this.tasksService.delete(id).pipe(takeUntil(this.destroyed$)).subscribe((response) => {
      linkElement.remove();
    });
  }

  editTask(model: any): void {
    const dialogRef = this.dialog.open(AddEditFormComponent, {
      width: '100%',
      maxWidth: '500px',
      data: {isCreate: false, taskModel: model}
    });

    dialogRef.afterClosed().pipe(takeUntil(this.destroyed$)).subscribe(result => {
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
