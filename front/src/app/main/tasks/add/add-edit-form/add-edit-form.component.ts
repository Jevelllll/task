import {Component, Inject, Input, OnDestroy, OnInit, Optional} from '@angular/core';
import {TaskModel} from '../../../../models/TaskModel';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidModel} from '../../../../models/AuthModel';
import {TasksService} from '../../../../services/task/tasks.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TasksComponent} from '../../tasks.component';

@Component({
  selector: 'app-add-edit-form',
  templateUrl: './add-edit-form.component.html',
  styleUrls: ['./add-edit-form.component.scss']
})
export class AddEditFormComponent implements OnInit, OnDestroy {

  @Input() isCreate = true;
  @Input() taskModel: TaskModel = new TaskModel(0, '', '');

  private destroyed$ = new Subject<void>();
  public titlesForm = {title: 'Create Task', titleBtn: 'Create'};

  public addTaskForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder, private taskService: TasksService, private router: Router,
    @Optional() public dialogRef: MatDialogRef<AddEditFormComponent | undefined>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: TasksComponent
  ) {
  }

  ngOnInit(): void {

    if (this.data) {
      // @ts-ignore
      this.taskModel = this.data.taskModel;
      // @ts-ignore
      this.isCreate = this.data.isCreate;
    }

    if (!this.isCreate) {
      this.titlesForm = {title: 'Update Task', titleBtn: 'Update'};
    }

    this.formInit();
  }

  private formInit(): void {

    this.addTaskForm = this.formBuilder.group({
      name: [
        this.taskModel.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(55)
        ])
      ],
      description: [
        this.taskModel.description,
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255)
        ])
      ]
    });
  }

  get name(): ValidModel {
    return this.addTaskForm.get('name')?.errors as ValidModel;
  }

  get description(): ValidModel {
    return this.addTaskForm.get('description')?.errors as ValidModel;
  }

  public onSubmit(): void {
    if (this.addTaskForm.invalid) {
      return;
    }
    if (this.isCreate) {
      this.taskService.addNewTask(this.addTaskForm.value)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((result => {
          if (result.id > 0) {
            this.router.navigateByUrl('/main/tasks');
          }
        }));
    } else {
      this.taskService.editTask(this.addTaskForm.value, this.taskModel.id)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((result) => {});
    }

  }

  closeDialog(): void {
    if (this.dialogRef) this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
