import {Component, OnDestroy, OnInit} from '@angular/core';
import {DistributedService} from '../../../services/distributed/distributed.service';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {DistributedModel} from '../../../models/DistributedModel';
import {switchMap, takeUntil} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {DeleteConfirmComponent} from '../../users/modals/delete-confirm/delete-confirm.component';
import {TasksService} from '../../../services/task/tasks.service';
import {TaskDistInterface, TaskStatus} from '../../../models/TaskModel';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-distributed',
  templateUrl: './distributed.component.html',
  styleUrls: ['./distributed.component.scss']
})
export class DistributedComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  public obsDistributed: Observable<DistributedModel[]> | undefined;
  public statusList: TaskStatus[] | undefined;

  public statusFormControl!: FormControl;

  constructor(private distributedService: DistributedService,
              private taskService: TasksService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {


    this.statusFormControl = new FormControl(0);
    this.statusFormControl.valueChanges.pipe(takeUntil(this.destroyed$)).subscribe((data) => {
      this.obsDistributed = new Observable<DistributedModel[]>();
      this.sendStatus(data);
    });


    this.taskService.obsEllTasksStatus.pipe(takeUntil(this.destroyed$)).subscribe((result) => {
      if (result.length > 0) {
        this.statusList = result;
      } else {
        this.taskService.getStatus().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }
    });

    this.distributedService.obsDistributed.pipe(takeUntil(this.destroyed$))
      .subscribe((result) => {
        if (result.length > 0) {
          this.obsDistributed = new BehaviorSubject<DistributedModel[]>(result).asObservable();
        } else {
          this.distributedService.getAll(1)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
            });
        }
      });
  }

  delete(data: DistributedModel, cardItem: HTMLElement): void {
    if (data) {
      const dialogRef = this.dialog.open(DeleteConfirmComponent,
        {
          width: '100%',
          maxWidth: '500px',
          data: {data}
        });
      dialogRef.afterClosed().pipe(switchMap((el) => {
        if (el) {
          return this.distributedService.delete(data.id);
        } else {
          return of(false);
        }
      })).subscribe((resp) => {
        if (resp !== false) {
          cardItem.remove();
        }
      });
    }
    // userModel
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  sendStatus(statusId: any): void {
    this.distributedService.getAll(statusId)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
      });
  }
}
