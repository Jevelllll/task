import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {of, Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth/auth.service';
import {LoginClass, RegistrationModel, ValidModel} from '../../../models/AuthModel';
import {switchMap, takeUntil} from 'rxjs/operators';
import {UsersService} from '../../../services/users/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reg-update-form',
  templateUrl: './reg-update-form.component.html',
  styleUrls: ['./reg-update-form.component.scss']
})
export class RegUpdateFormComponent implements OnInit, OnDestroy {

  // tslint:disable-next-line:no-input-rename
  @Input('regForm') regForm: any;
  @Input('isAdminEdit') isAdminEdit = true;
  @Output() onChangedUpdate = new EventEmitter<boolean>();

  public titleButton = 'Sign up';
  private RegistrationModel = new RegistrationModel('', '', '');

  private destroyed$ = new Subject<void>();
  public registerForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UsersService,
    private router: Router
  ) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {

    this.formInit();
    if (this.regForm) {
      this.titleButton = 'Update';
      this.RegistrationModel.email = this.regForm.email;
      this.RegistrationModel.nickname = this.regForm.nickname;
      this.registerForm.get('email')?.setValue(this.regForm.email);
      this.registerForm.get('nickname')?.setValue(this.regForm.nickname);
    }
  }

  private formInit(): void {

    if (this.regForm) {
      this.registerForm = this.formBuilder.group({
        email: [this.RegistrationModel.email,
          Validators.compose([
            Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')
          ])],
        nickname: [this.RegistrationModel.nickname,
          Validators.compose([
            Validators.minLength(4),
            Validators.maxLength(24)
          ])
        ],
        plainPassword: [this.RegistrationModel.plainPassword,
          Validators.compose([Validators.minLength(4)
          ])],
        confirmPlainPassword: [this.RegistrationModel.plainPassword,
          Validators.compose([Validators.minLength(4)
          ])],
      }, {validators: this.passwordsMatchValidator('plainPassword', 'confirmPlainPassword')});
    } else {
      this.registerForm = this.formBuilder.group({
        email: [this.RegistrationModel.email,
          Validators.compose([
            Validators.required,
            Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')
          ])],
        nickname: [this.RegistrationModel.nickname,
          Validators.compose([
            Validators.minLength(4),
            Validators.maxLength(24)
          ])
        ],
        plainPassword: [this.RegistrationModel.plainPassword,
          Validators.compose([
            Validators.required,
            Validators.minLength(4)
          ])],
        confirmPlainPassword: [this.RegistrationModel.plainPassword,
          Validators.compose([
            Validators.required,
            Validators.minLength(4)
          ])],
      }, {validators: this.passwordsMatchValidator('plainPassword', 'confirmPlainPassword')});
    }

  }

  // tslint:disable-next-line:typedef
  private passwordsMatchValidator(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      // tslint:disable-next-line:one-variable-per-declaration
      const passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true});
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  // tslint:disable-next-line:typedef
  get nickname(): ValidModel {
    return this.registerForm.get('nickname')?.errors as ValidModel;
  }

  // tslint:disable-next-line:typedef
  get password(): ValidModel {
    return this.registerForm.get('plainPassword')?.errors as ValidModel;
  }

  // tslint:disable-next-line:typedef
  get confirmPassword(): ValidModel {
    return this.registerForm.get('confirmPlainPassword')?.errors as ValidModel;
  }

  public onSubmit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    if (this.regForm) {
      this.userService.edit(this.registerForm.value, this.regForm.id, this.isAdminEdit)
        .pipe(takeUntil(this.destroyed$))
        .subscribe((result) => {
          this.onChangedUpdate.emit(true);
        });
    } else {
      this.authService.registrationData(this.registerForm.value)
        .pipe(takeUntil(this.destroyed$), switchMap((dataSwitch: any) => {
          if (dataSwitch) {
            return this.authService.login(
              new LoginClass(this.registerForm.value.email, this.registerForm.value.plainPassword)
            );
          } else {
            return of(false);
          }
        }))
        .subscribe((result) => {
        });
    }
  }
}
