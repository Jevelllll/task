import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegUpdateFormComponent} from './reg-update-form.component';
import {MaterialModule} from '../../../shared/material.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    RegUpdateFormComponent
  ],
  exports: [
    RegUpdateFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class ReqUpdateFormModule { }
