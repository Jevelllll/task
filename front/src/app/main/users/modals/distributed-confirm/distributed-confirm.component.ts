import {Component, Inject, OnInit, Optional} from '@angular/core';
import {TasksService} from '../../../../services/task/tasks.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UsersComponent} from '../../users.component';
import {RegistrationModelResponse} from '../../../../models/AuthModel';
import {TaskDistInterface, TaskInterface} from '../../../../models/TaskModel';
import {Observable} from 'rxjs';
import {DistributedService} from '../../../../services/distributed/distributed.service';
import {DistributedModelAdd} from '../../../../models/DistributedModel';

@Component({
  selector: 'app-distributed-confirm',
  templateUrl: './distributed-confirm.component.html',
  styleUrls: ['./distributed-confirm.component.scss']
})
export class DistributedConfirmComponent implements OnInit {

  public userModel: RegistrationModelResponse | undefined;
  public obsTasks: Observable<TaskDistInterface[]> | undefined;

  constructor(private tasksService: TasksService, private distributesService: DistributedService,
              @Optional() public dialogRef: MatDialogRef<UsersComponent | undefined>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: UsersComponent) {
  }

  ngOnInit(): void {
    this.userModel = this.data.userModel;
    this.obsTasks = this.tasksService.getTasksFry(this.userModel?.id);
  }

  assing(taskId: number, taskLink: HTMLElement): void {
    if (this.userModel?.id) {
      const sendData = new DistributedModelAdd(taskId, this.userModel?.id);
      this.distributesService.add(sendData).subscribe(() => {
        taskLink.remove();
      });
    }
  }
}
