import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UsersComponent} from '../../users.component';
import {RegistrationModelResponse} from '../../../../models/AuthModel';
import {Subject} from 'rxjs';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-update-confirm',
  templateUrl: './update-confirm.component.html',
  styleUrls: ['./update-confirm.component.scss']
})
export class UpdateConfirmComponent implements OnInit {

  public userModel: RegistrationModelResponse | undefined;
  public isAdminEdit = true;

  private destroyed$ = new Subject<void>();
  public updateUser!: FormGroup;

  constructor(@Optional() public dialogRef: MatDialogRef<UsersComponent | undefined>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: UpdateConfirmComponent) {
  }


  ngOnInit(): void {
    this.isAdminEdit = this.data.isAdminEdit;
    this.userModel = this.data.userModel;
  }

  onChanged(event: boolean): void {
    this.dialogRef.close();
  }
}
