import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UsersComponent} from '../../users.component';
import {RegistrationModelResponse} from '../../../../models/AuthModel';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
  styleUrls: ['./delete-confirm.component.scss']
})
export class DeleteConfirmComponent implements OnInit {

  public userModel: RegistrationModelResponse | undefined;

  constructor(@Optional() public dialogRef: MatDialogRef<UsersComponent | undefined>,
              @Optional() @Inject(MAT_DIALOG_DATA) public data: DeleteConfirmComponent) {
  }

  ngOnInit(): void {
    this.userModel = this.data.userModel;
  }

}
