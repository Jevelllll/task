import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../../services/users/users.service';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {RegistrationModelResponse} from '../../models/AuthModel';
import {AuthService} from '../../services/auth/auth.service';
import {map, switchMap, takeUntil} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {DeleteConfirmComponent} from './modals/delete-confirm/delete-confirm.component';
import {UpdateConfirmComponent} from './modals/update-confirm/update-confirm.component';
import {DistributedConfirmComponent} from './modals/distributed-confirm/distributed-confirm.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  public obsUsers: Observable<RegistrationModelResponse[]> | undefined;
  public isAdmin = false;
  userModel: RegistrationModelResponse | undefined;

  constructor(private usersService: UsersService, private authService: AuthService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();

    this.usersService.obsEllUsers.pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        if (data.length > 0) {
          this.obsUsers = new BehaviorSubject<RegistrationModelResponse[]>(data).asObservable();
        } else {
          this.usersService.getAll()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
            });
        }
      });

  }

  public deleteUser(userModel: RegistrationModelResponse, cardItem: HTMLElement): void {
    if (userModel) {
      const dialogRef = this.dialog.open(DeleteConfirmComponent,
        {
          width: '100%',
          maxWidth: '500px',
          data: {userModel}
        });
      dialogRef.afterClosed().pipe(switchMap((el) => {
        if (el) {
          return this.usersService.delete(userModel.id);
        } else {
          return of(false);
        }
      })).subscribe((resp) => {
        if (resp !== false) {
          cardItem.remove();
        }
      });
    }
  }

  public editUser(userModel: RegistrationModelResponse): void {
    if (userModel) {
      const dialogRef = this.dialog.open(UpdateConfirmComponent,
        {
          width: '100%',
          maxWidth: '500px',
          data: {userModel, isAdminEdit: true}
        });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }

  public addTask(userModel: RegistrationModelResponse): void {
    if (userModel) {
      const dialogRef = this.dialog.open(DistributedConfirmComponent,
        {
          width: '100%',
          maxWidth: '500px',
          data: {userModel}
        });
      dialogRef.afterClosed().subscribe(result => {
      });
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
