import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainComponent} from '../main.component';
import {AuthGuardService} from '../../services/guard/auth-guard.service';
import {UsersComponent} from '../users/users.component';
import {TasksComponent} from '../tasks/tasks.component';
import {AddComponent} from '../tasks/add/add.component';
import {RoleGuardService} from '../../services/guard/role-guard.service';
import {DistributedComponent} from '../tasks/distributed/distributed.component';
import {StartedComponent} from '../started/started/started.component';

const itemRoutes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [RoleGuardService],
    data: {role: 'ROLE_ADMIN'}
  },
  {
    path: 'tasks',
    component: TasksComponent,
    canActivate: [RoleGuardService],
    data: {role: 'ROLE_ADMIN'}
  },
  {
    path: 'tasks/add',
    component: AddComponent,
    canActivate: [RoleGuardService],
    data: {role: 'ROLE_ADMIN'}
  },
  {
    path: 'distributed',
    component: DistributedComponent,
    canActivate: [RoleGuardService],
    data: {role: 'ROLE_ADMIN'}
  },
  {
    path: 'started-task',
    component: StartedComponent
  },
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
    canActivate: [RoleGuardService],
    data: {role: 'ROLE_ADMIN'}
  },
];

const routes: Routes = [
  {
    path: '', component: MainComponent, canActivate: [AuthGuardService], children: itemRoutes
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
