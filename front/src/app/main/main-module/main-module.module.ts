import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MainComponent} from '../main.component';
import {MainRoutingModule} from './main-routing.module';
import {FooterComponent} from '../../footer/footer.component';
import {TopBarComponent} from '../../top-bar/top-bar.component';
import {MaterialModule} from '../../shared/material.module';
import {UsersComponent} from '../users/users.component';
import {AddComponent} from '../tasks/add/add.component';
import {AddEditFormComponent} from '../tasks/add/add-edit-form/add-edit-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TasksService} from '../../services/task/tasks.service';
import {TasksComponent} from '../tasks/tasks.component';
import {MatDialogModule} from '@angular/material/dialog';
import {DistributedComponent} from '../tasks/distributed/distributed.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {DeleteConfirmComponent} from '../users/modals/delete-confirm/delete-confirm.component';
import {UpdateConfirmComponent} from '../users/modals/update-confirm/update-confirm.component';
import {DistributedConfirmComponent} from '../users/modals/distributed-confirm/distributed-confirm.component';
import {ReqUpdateFormModule} from '../users/reg-update-form/req-update-form.module';
import {DistributedService} from '../../services/distributed/distributed.service';
import {MatRadioModule} from '@angular/material/radio';
import {StartedComponent} from '../started/started/started.component';
import {MatSelectModule} from "@angular/material/select";
import {MatMenuModule} from "@angular/material/menu";

@NgModule({
  declarations: [
    MainComponent,
    FooterComponent,
    TopBarComponent,
    UsersComponent,
    AddComponent,
    TasksComponent,
    AddEditFormComponent,
    DistributedComponent,
    DeleteConfirmComponent,
    UpdateConfirmComponent,
    DistributedConfirmComponent,
    StartedComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    MaterialModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatExpansionModule,
    ReqUpdateFormModule,
    MatRadioModule,
    MatSelectModule,
    MatMenuModule
  ],
  providers: [
    TasksService, DistributedService
  ],
  exports: [
    MainRoutingModule
  ]
})
export class MainModuleModule {
}
