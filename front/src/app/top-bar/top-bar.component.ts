import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../services/auth/auth.service';
import {sidenavData} from '../shared/data-sidenav';
import {MatDialog} from '@angular/material/dialog';
import {UsersService} from '../services/users/users.service';
import {of, Subject} from 'rxjs';
import {takeUntil} from "rxjs/operators";
import {flatMap} from "rxjs/internal/operators";
import {UpdateConfirmComponent} from "../main/users/modals/update-confirm/update-confirm.component";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  public panelOpenState = [{}];
  public sidenavData;
  isAdmin = false;
  public username: string | undefined;

  constructor(private authService: AuthService, public dialog: MatDialog, private userService: UsersService) {
    this.sidenavData = sidenavData;
    this.isAdmin = authService.isAdmin();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.sidenavData.length; i++) {
      // @ts-ignore
      this.panelOpenState.push(false);
    }
  }

  openDialog(): void {
    this.userService.myProfile()
      .pipe(takeUntil(this.destroyed$), flatMap((userModel) => {
        if (userModel) {
          const dialogRef = this.dialog.open(UpdateConfirmComponent,
            {
              width: '100%',
              maxWidth: '500px',
              data: {userModel, isAdminEdit: false}
            });
          return dialogRef.afterClosed();
        } else {
          return of(false);
        }
      }))
      .subscribe((result) => {
      });
  }

  ngOnInit(): void {
    this.username = this.authService.getUserName();
  }

  public logout(): void {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
