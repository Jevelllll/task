import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidModel} from '../../models/AuthModel';
import {AuthService} from '../../services/auth/auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private destroyed$ = new Subject<void>();
  // @ts-ignore
  public loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.formInit();
  }

  private formInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [null,
        Validators.compose([
          Validators.required,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')
        ])],
      password: [null,
        Validators.compose([
          Validators.required,
          Validators.minLength(4)
        ])]
    });
  }

  get passwordError(): ValidModel {
    return this.loginForm.get('password')?.errors as ValidModel;
  }

  public onSubmit(): void {
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.loginForm.value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
