import {RouterModule, Routes} from '@angular/router';
import {AuthComponent} from '../auth.component';
import {NgModule} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {ReqUpdateFormModule} from '../../main/users/reg-update-form/req-update-form.module';

const routes: Routes = [
  {
    path: '', component: AuthComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ReqUpdateFormModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService
  ]
})
export class AuthRoutingModule {
}
