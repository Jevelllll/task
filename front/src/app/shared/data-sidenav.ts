export let sidenavData = [
  {
    title: 'User',
    ROLE_ADMIN: true,
    children: [
      {
        title: 'List',
        icon: 'supervisor_account',
        link: '/main/users'
      }
    ]
  },
  {
    title: 'Task',
    ROLE_ADMIN: true,
    children: [
      {
        title: 'New',
        icon: 'add_task',
        link: '/main/tasks/add'
      },
      {
        title: 'List',
        icon: 'list_alt',
        link: '/main/tasks'
      },
    ]
  },
  {
    title: 'Distributed',
    ROLE_ADMIN: true,
    children: [
      {
        title: 'List',
        icon: 'list_alt',
        link: '/main/distributed'
      },
    ]
  }
];
