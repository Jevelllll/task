import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {map, takeUntil, tap} from 'rxjs/operators';
import {RegistrationInterface, RegistrationModelResponse} from '../../models/AuthModel';

@Injectable({providedIn: 'root'})
export class UsersService implements OnDestroy {

  private url: string = environment.api_url;
  private destroyed$ = new Subject<void>();
  private behEllUsers!: BehaviorSubject<RegistrationModelResponse[]>;
  public obsEllUsers: Observable<RegistrationModelResponse[]> = new Observable<RegistrationModelResponse[]>();

  constructor(private http: HttpClient) {
    this.behEllUsers = new BehaviorSubject<RegistrationModelResponse[]>([]);
    this.obsEllUsers = this.behEllUsers.asObservable();
  }

  public getAll(): Observable<RegistrationModelResponse[]> {

    return this.http.get<RegistrationModelResponse[]>(`${this.url}users`).pipe(
      map((el: RegistrationModelResponse[]) => el.filter((it: { roles: string | string[]; }) => it.roles.indexOf('ROLE_ADMIN') === -1)),
      tap(resp => {
        if (resp.length > 0) {
          this.behEllUsers.next(resp);
        }
      })
    );
  }

  public edit(data: RegistrationInterface, userId: number, isAdminEdit = false): Observable<RegistrationInterface> {
    return this.http.put<any>(`${this.url}users/${userId}`, data, {responseType: 'json'})
      .pipe(tap((item) => {
        if (isAdminEdit) {
          this.getAll().pipe(takeUntil(this.destroyed$)).subscribe(() => {
          });
        }
      }));
  }

  public myProfile(): Observable<RegistrationInterface> {
    return this.http.get<any>(`${this.url}users/profile`, {responseType: 'json'});
  }


  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.url}users/${id}`);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
