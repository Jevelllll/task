import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {
  LoginModel,
  LoginModelResponse,
  RegistrationInterface,
  RegistrationModelResponse
} from '../../models/AuthModel';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthService {

  private url: string = environment.api_url;

  constructor(private http: HttpClient, private route: Router) {
  }

  public registrationData(data: RegistrationInterface): Observable<RegistrationModelResponse> {
    return this.http.post<RegistrationModelResponse>(
      `${this.url}register`, data, {responseType: 'json'}
    );
  }

  public login(data: LoginModel): Observable<LoginModelResponse> {
    return this.http.post<LoginModelResponse>(`${this.url}login`, data, {responseType: 'json'})
      .pipe(
        tap(response => {
          if (response) {
            localStorage.setItem('jwt', JSON.stringify(response.token));
            const roles = this.decodeToken(response.token);
            if (roles.indexOf('ROLE_ADMIN') > -1) {
              this.route.navigateByUrl('/main/users');
            } else {
              this.route.navigateByUrl('/main/started-task');
            }
          }
        })
      );
  }

  public isAdmin(): boolean {

    let isAdmin = [];
    const helper = new JwtHelperService();
    try {
      const roles = helper.decodeToken(localStorage.getItem('jwt') as string).roles;
      // @ts-ignore
      isAdmin = roles.filter((it) => it.indexOf('ROLE_ADMIN') > -1);
    } catch (e) {
    }
    return isAdmin.length > 0;
  }

  public getUserID(): number {
    let userId = null;
    const helper = new JwtHelperService();
    try {
      userId = helper.decodeToken(localStorage.getItem('jwt') as string).user_id;
    } catch (e) {
    }
    return userId;
  }

  public getUserName(): string {
    let username = null;
    const helper = new JwtHelperService();
    try {
      username = helper.decodeToken(localStorage.getItem('jwt') as string).username;
    } catch (e) {
    }
    return username;
  }

  private decodeToken(token: string = ''): string {
    let roles = [];
    const helper = new JwtHelperService();
    if (token.length > 1) {
      localStorage.setItem('jwt', JSON.stringify(token));
      const decodedToken = helper.decodeToken(token);
      roles = decodedToken.roles;
    } else {
      try {
        roles = helper.decodeToken(localStorage.getItem('jwt') as string).roles;
      } catch (e) {
      }
    }
    return roles;
  }

  public logout(): void {
    // this.behaviorRole.next([]);
    localStorage.removeItem('jwt');
    this.route.navigateByUrl('/auth');
  }
}
