import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {DistributedModel, DistributedModelAdd, DistributedResponse} from '../../models/DistributedModel';

@Injectable()
export class DistributedService implements OnDestroy {

  private url: string = environment.api_url;
  private destroyed$ = new Subject<void>();

  private behDistributed!: BehaviorSubject<DistributedModel[]>;
  public obsDistributed: Observable<DistributedModel[]> = new Observable<DistributedModel[]>();

  constructor(private http: HttpClient) {
    this.behDistributed = new BehaviorSubject<DistributedModel[]>([]);
    this.obsDistributed = this.behDistributed.asObservable();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  getAll(taskId = 0): Observable<DistributedModel[]> {
    return this.http.get<any>(`${this.url}distributed/task/${taskId}`).pipe(tap((resp) => {
      if (resp.length > 0) {
        this.behDistributed.next(resp);
      }
    }));
  }

  public add(data: DistributedModelAdd): Observable<DistributedResponse> {
    return this.http.post<DistributedResponse>(`${this.url}distributed/add`, data, {responseType: 'json'});
  }

  // tslint:disable-next-line:variable-name
  public changeStatus(distributedID: number, status_id: number): Observable<DistributedResponse> {
    return this.http.put<DistributedResponse>(`${this.url}distributed/change-status/${distributedID}`, {status_id});
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.url}distributed/remove/${id}`);
  }
}
