import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Subject} from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate, OnDestroy {

  private destroyed$ = new Subject<void>();

  constructor(private authService: AuthService) {
  }

  // tslint:disable-next-line:typedef
  canActivate(route: ActivatedRouteSnapshot) {
    const role = route.data.role;
    const helper = new JwtHelperService();
    let currentRole = [];
    try {
      currentRole = helper.decodeToken(localStorage.getItem('jwt') as string).roles;
    } catch (e) {
    }

    if (currentRole.length > 0) {
      if (role && currentRole.indexOf(role) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
