import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(): boolean {

    if (localStorage.getItem('jwt')) {
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page
    this.router.navigate(['auth']);
    return false;
  }

  checkLogin(): boolean {
    if (localStorage.getItem('jwt')) {
      // logged in so return true
      return true;
    }
    return false;
  }

}
