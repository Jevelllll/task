import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {TaskInterface, TaskModel, TaskStatus} from '../../models/TaskModel';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {DistributedModel} from '../../models/DistributedModel';

@Injectable()
export class TasksService implements OnDestroy {

  private url: string = environment.api_url;

  private destroyed$ = new Subject<void>();
  private behEllTasks!: BehaviorSubject<TaskInterface[]>;
  public obsEllTasks: Observable<TaskInterface[]> = new Observable<TaskInterface[]>();

  private behEllTasksStatus!: BehaviorSubject<TaskStatus[]>;
  public obsEllTasksStatus: Observable<TaskStatus[]> = new Observable<TaskStatus[]>();

  private behEllTaskStarted!: BehaviorSubject<DistributedModel[]>;
  public obsEllTasksStarted: Observable<DistributedModel[]> = new Observable<DistributedModel[]>();

  constructor(private http: HttpClient, private auth: AuthService) {
    this.behEllTasks = new BehaviorSubject<TaskInterface[]>([]);
    this.obsEllTasks = this.behEllTasks.asObservable();

    this.behEllTasksStatus = new BehaviorSubject<TaskStatus[]>([]);
    this.obsEllTasksStatus = this.behEllTasksStatus.asObservable();

    this.behEllTaskStarted = new BehaviorSubject<DistributedModel[]>([]);
    this.obsEllTasksStarted = this.behEllTaskStarted.asObservable();
  }

  public addNewTask(data: TaskModel): Observable<TaskModel> {
    return this.http.post<TaskModel>(`${this.url}tasks`, data, {responseType: 'json'})
      .pipe(tap((item) => {
        this.getAllTasks().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }));
  }

  public getAllTasks(): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(`${this.url}tasks`).pipe(tap((resp) => {
      if (resp.length > 0) {
        this.behEllTasks.next(resp);
      }
    }));
  }

  public getTasksFry($userId: number): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(`${this.url}tasks/free-tasks/${$userId}`);
  }

  public getStarted(statusId: number): Observable<DistributedModel[]> {
    return this.http.get<DistributedModel[]>(`${this.url}tasks/started/${this.auth.getUserID()}/${statusId}`)
      .pipe(tap(el => {
        if (el.length > 0) {
          this.behEllTaskStarted.next(el);
        }
      }));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.url}tasks/${id}`);
  }

  // public getTaskByUserId($userId) {
  //
  // }

  public editTask(data: TaskModel, id: number): Observable<any> {
    return this.http.put<any>(`${this.url}tasks/${id}`, data, {responseType: 'json'})
      .pipe(tap((item) => {
        this.getAllTasks().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }), takeUntil(this.destroyed$));
  }

  public getStatus(): Observable<TaskStatus[]> {
    return this.http.get<TaskStatus[]>(`${this.url}task_statuses`)
      .pipe(tap((item) => {
        this.behEllTasksStatus.next(item);
      }), takeUntil(this.destroyed$));
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
