export interface ValidModel {
  minlength: RequiredLength;
  maxlength: RequiredLength;
  required: string;
  notEquivalent: boolean;
}

export interface RequiredLength {
  requiredLength: string;
}

export interface RegistrationInterface {
  email: string;
  nickname: string;
  plainPassword: string;
}

export class RegistrationModel {
  constructor(
    public email: string,
    public nickname: string,
    public plainPassword: string,
  ) {
  }

}

export interface RegistrationModelResponse {
  id: any;
  email: string;
  nickname: string;
  roles: [
    string
  ];
  password: string;
  createdAt: string;
  updatedAt: string;
}

export interface LoginModel {
  username: string;
  password: string;
}

export class LoginClass {
  constructor(public username: string, public password: string) {
  }
}

export interface LoginModelResponse {
  token: string;
}
