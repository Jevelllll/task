export class TaskModel {
  constructor(
    public id: number,
    public name: string,
    public description: string,
  ) {
  }
}

export interface TaskInterface {
  id: number;
  name: string;
  description: string;
  created_at: string;
  updated_at: string;
}

export interface TaskDistInterface {
  id: number;
  name: string;
  description: string;
  created_at: any;
}

export interface TaskStatus {
  id: number;
  name: string;
}
