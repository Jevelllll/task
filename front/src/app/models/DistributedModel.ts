export interface DistributedModel {
  created_at: any;
  description: string;
  email: string;
  id: number;
  nickname: string;
  status_id: number;
  status_name: string;
  task_id: number;
  task_name: string;
  user_id: number;
}

export class DistributedModelAdd {
  // tslint:disable-next-line:variable-name
  constructor(public task_id: number, public user_id: number) {
  }
}

export interface DistributedResponse {
  status: string;
}
